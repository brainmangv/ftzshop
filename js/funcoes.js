$(document).mouseup(function (e){
  var container = $("#input-login");
  if (!container.is(e.target) 
      && container.has(e.target).length === 0){
       $('#input-lock').animate({top:0},function(){$('#social-buttons').show();})          
  }
  var container = $("#menu-list");
  var container2 = $(".cd-dropdown");
  if ((!container.is(e.target)&& container.has(e.target).length === 0)&&
      (!container2.is(e.target) && container2.has(e.target).length === 0)){
       $('#input-lock').animate({top:0},function(){$('.cd-dropdown').hide();})          
  }
});     


$(document).ready(function() {
  var originalFontSize = 12;
  var sectionWidth = $('.inner-clock').width();

  $('.inner-clock span').each(function(){
    var spanWidth = $(this).width();
    var newFontSize = (sectionWidth/spanWidth) * originalFontSize;
    $(this).css({"font-size" : newFontSize, "line-height" : newFontSize/1.2 + "px"});
  });

  $( '#cd-dropdown' ).dropdown( {
    gutter : 5,
    delay : 100,
    random : true
  } );

  $('#menu-list').on('click',function(){
   $(".cd-dropdown").show('slow');
  })
  $('.cd-dropdown').mouseout(function(){
   $(".cd-dropdown").hide('slow');
  });

  $('#input-login').on('click',function(){  
      $('#social-buttons').hide();
      $('#input-lock').animate({top:35})
  });

  //--------------secao destaque--------------
  $("#destaque .product-wrapper > .image").on('click', function(){
  var c = $(this).parent().attr("class").split(/\s+/)[1];                     
    if (c!="active"){   
    $("#destaque .product-wrapper").not(this).each(function(){
      $(this).animate({width: 185});
      $(this).removeClass("active");
    });
    $(this).parent().animate({width: 340});
    $(this).parent().addClass('active');
    }
  });

  //---------------secao exclusivas-----------
  $("#exclusivas .product-wrapper > .image").on('click', function(){
     var c = $(this).parent().attr("class").split(/\s+/)[1];              
    // if ($(this).parent().css('width') == '185px'){
    if (c!="active"){   
      $("#exclusivas .product-wrapper").not(this).each(function(){                
        $(this).animate({width: 185});         
        $(this).switchClass('active','desative');
      })              

      $(this).parent().animate({width: 490},
        function(){$(this).switchClass('desactive','active');} 
      );             
    }
  });  

  //----------------------------------------------
  // inicializa o relogio da secao exlusivos

  $('.chart').easyPieChart({
  easing: 'easeOutElastic',
        delay: 3000,
        lineWidth: 24,
        barColor: '#ADFF2F',
        trackColor: false,
        scaleColor: false,
        lineCap: 'but',
        size:150
  });
  $('.small-chart').easyPieChart({
  easing: 'easeOutElastic',
        delay: 3000,
        lineWidth: 15,
        barColor: '#ADFF2F',
        trackColor: false,
        scaleColor: false,
        lineCap: 'but',
        size:50
  });

});