var o = {
	init: function(d){
		this.diagram(d);
	},
	random: function(l, u){
		return Math.floor((Math.random()*(u-l+1))+l);
	},
	diagram: function(dobj){

		var size = 150;
			r = Raphael(dobj, size, size),
			middle= size/2;
			rad = 22,
			defaultText = 'Avaliações',
			speed = 250;
		r.circle(middle, middle, 75).attr({ stroke: 'solid'});

		r.circle(middle, middle, 30).attr({ stroke: 'none', fill: '#193340' });
		
		var title = r.text(middle, middle, defaultText).attr({
			font: '10px Arial',
			fill: '#fff'
		}).toFront();
		
		r.customAttributes.arc = function(value, color, rad){
			var v = 3.6*value,
				alpha = v == 360 ? 359.99 : v,
				random = o.random(91, 240),
				a = (random-alpha) * Math.PI/180,
				b = random * Math.PI/180,
				sx = middle + rad * Math.cos(b),
				sy = middle - rad * Math.sin(b),
				//sx=middle+rad,sy=middle-rad,
				x = middle + rad * Math.cos(a),
				y = middle - rad * Math.sin(a),
				path = [['M', sx, sy], ['A', rad, rad, 0, +(alpha > 180), 1, x, y]];
			return { path: path, stroke: color }
		}
		
		$(dobj).find('.get').find('.arc').each(function(i){
			var t = $(this), 
				color = t.find('.color').val(),
				value = t.find('.percent').val(),
				text = t.find('.text').text();
			
			rad += 12;	
			var z = r.path().attr({ arc: [value, color, rad], 'stroke-width': 10 });
			
			z.mouseover(function(){
                this.animate({ 'stroke-width': 15, opacity: .75 }, 1000, 'elastic');
                if(Raphael.type != 'VML') //solves IE problem
				this.toFront();
				title.stop().animate({ opacity: 0 }, speed, '>', function(){
					this.attr({ text: text + '\n' + value + '%' }).animate({ opacity: 1 }, speed, '<');
				});
            }).mouseout(function(){
				this.stop().animate({ 'stroke-width': 10, opacity: 1 }, speed*4, 'elastic');
				title.stop().animate({ opacity: 0 }, speed, '>', function(){
					title.attr({ text: defaultText }).animate({ opacity: 1 }, speed, '<');
				});	
            });
		});
		
	}
}
